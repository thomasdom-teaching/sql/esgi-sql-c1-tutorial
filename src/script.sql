-- Disconnect from current database and connect to postgres for maintenance
\c postgres

-- Recreate fresh database
DROP DATABASE IF EXISTS sql_c1_tutorial;

DROP ROLE mccree;

CREATE ROLE mccree SUPERUSER;

CREATE DATABASE sql_c1_tutorial OWNER postgres;

-- Reconnect to current database
\c sql_c1_tutorial

-- Schema
CREATE TABLE can (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    capacity_cl INTEGER NOT NULL CHECK (capacity_cl > 10)
);

CREATE TABLE student (
    login VARCHAR(8) PRIMARY KEY,
    firstname VARCHAR(64) NOT NULL,
    lastname VARCHAR(64) NOT NULL,
    gender BOOLEAN NOT NULL DEFAULT FALSE -- 0 if it's a boy, 1 if it's a girl
);

CREATE TABLE student_can (
    id SERIAL PRIMARY KEY,
    student_login VARCHAR(8) NOT NULL REFERENCES student (login) ON DELETE CASCADE,
    can_id INTEGER NOT NULL REFERENCES can (id) ON DELETE CASCADE
);

-- Data fixtures
INSERT INTO can (name, capacity_cl)
VALUES 
    ('Coke', 25),
    ('Oasis', 25),
    ('Diet Coke', 25),
    ('Orangina', 25),
    ('Sprite', 25),
    ('Ice Tea', 25),
    ('Pepsi', 25),
    ('Water', 50),
    ('Fanta', 25),
    ('Orange Juice', 33);

INSERT INTO student
VALUES
    ('nbarray', 'Nicolas', 'Barray', FALSE),
    ('wbekhtao', 'Walid', 'Bekhtaoui', FALSE),
    ('rbillon', 'Rémi', 'Billon', FALSE),
    ('abitar', 'Alex', 'Bitar', FALSE),
    ('qcoelho', 'Quentin', 'Coelho', FALSE),
    ('adavray', 'Arthur', 'D''avray', FALSE),
    ('pdagues', 'Pierre-Louis', 'Dagues', FALSE),
    ('jdonnett', 'Jean-Baptiste', 'Donnette', FALSE),
    ('agaillar', 'Arnaud', 'Gaillard', FALSE),
    ('rgozlan', 'Rafael', 'Gozlan', FALSE),
    ('lgroux', 'Louis', 'Groux', FALSE),
    ('aguiho', 'Alexis', 'Guiho', FALSE),
    ('nlayet', 'Nils', 'Layet', FALSE),
    ('qlhours', 'Quentin', 'L''Hours', FALSE),
    ('llubrano', 'Luc-Junior', 'Lubrano-Lavadera', FALSE),
    ('hmaurer', 'Hugo', 'Maurer', FALSE),
    ('aou', 'Arnaud', 'Ou', FALSE),
    ('spiat', 'Sébastien', 'Piat', FALSE),
    ('tsitum', 'Tania', 'Situm', TRUE),
    ('ataing', 'Anais', 'Taing', TRUE),
    ('bteixeir', 'Brian', 'Teixeira', FALSE),
    ('gthepaut', 'Guillaume', 'Thepaut', FALSE),
    ('atoubian', 'Adrien', 'Toubiana', FALSE),
    ('ctresarr', 'Côme', 'Tresarrieu', FALSE),
    ('pveyry', 'Pierre-Alexandre', 'Veyry', FALSE),
    ('fvisoiu', 'Francis', 'Visoiu', FALSE);

INSERT INTO student_can (student_login, can_id)
VALUES
    ('nbarray', 1),
    ('ataing', 8),
    ('gthepaut', 4),
    ('lgroux', 2),
    ('nbarray', 7),
    ('llubrano', 1),
    ('ctresarr', 3),
    ('pveyry', 4),
    ('aguiho', 10),
    ('adavray', 9);
